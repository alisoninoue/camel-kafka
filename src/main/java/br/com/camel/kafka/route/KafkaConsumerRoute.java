package br.com.camel.kafka.route;

import br.com.camel.kafka.avro.Payment;
import br.com.camel.kafka.serde.CustomKafkaAvroDeserializer;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaConstants;
import org.apache.camel.component.kafka.KafkaManualCommit;
import org.apache.camel.processor.aggregate.GroupedBodyAggregationStrategy;
import org.apache.camel.processor.aggregate.GroupedExchangeAggregationStrategy;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.stereotype.Component;

//Rota Desativada
//@Component
public class KafkaConsumerRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        from("kafka:transactions?brokers=localhost:9092"
                + "&groupId=kafkaGroup"
                + "&consumersCount=1"
                + "&keyDeserializer=" + StringDeserializer.class.getName()
                + "&valueDeserializer=" + CustomKafkaAvroDeserializer.class.getName()
                + "&allowManualCommit=true"
                + "&autoCommitEnable=false"
                + "&kafkaHeaderDeserializer=#kafkaHeaderDes"
                + "&autoOffsetReset=earliest")
                .aggregate(constant("1"), new GroupedBodyAggregationStrategy()).completionSize(3)
                .log("${header.header}")
                .process(exchange -> {
                    // manually commit offset if it is last message in batch
                    Boolean lastOne = exchange.getIn().getHeader(KafkaConstants.LAST_RECORD_BEFORE_COMMIT, Boolean.class);
                    Payment header = exchange.getIn().getHeader("header", Payment.class);

                    if (lastOne) {
                        KafkaManualCommit manual =
                                exchange.getIn().getHeader(KafkaConstants.MANUAL_COMMIT, KafkaManualCommit.class);
                        if (manual != null) {
                            System.out.println("manually committing the offset for batch");
                            manual.commitSync();
                        }
                    } else {
                        System.out.println("NOT time to commit the offset yet");
                    }
                })
                .log("${headers} ${body}");
    }
}
