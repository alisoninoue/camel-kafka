package br.com.camel.kafka.route;

import br.com.camel.kafka.avro.Payment;
import br.com.camel.kafka.serde.CustomKafkaAvroSerializer;
import org.apache.camel.builder.RouteBuilder;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.UUID;

@Component
public class KafkaProducerHeaderRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        from("timer:teste?delay=2000&repeatCount=5")
                .process(exchange -> {
                    Payment payment = new Payment(UUID.randomUUID().toString(), new Random().nextDouble());
                    exchange.getIn().setBody(payment);
                    exchange.getIn().setHeader("header", payment); //setado o header com o mesmo avro do body para teste
                })
                .log("Producer => ${body}")
                .setHeader("teste", constant("alison"))
                .to("kafka:transactions?brokers=localhost:9092"
                        + "&serializerClass=" + CustomKafkaAvroSerializer.class.getName()
                        + "&keySerializerClass=" + StringSerializer.class.getName()
                        + "&kafkaHeaderSerializer=#kafkaHeaderSer"
                );
    }
}
