package br.com.camel.kafka.route;

import br.com.camel.kafka.avro.Payment;
import br.com.camel.kafka.serde.CustomKafkaAvroDeserializer;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaConstants;
import org.apache.camel.component.kafka.KafkaManualCommit;
import org.apache.camel.processor.aggregate.GroupedBodyAggregationStrategy;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumerHeaderRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        from("kafka:transactions?brokers=localhost:9092"
                + "&groupId=kafkaGroup"
                + "&consumersCount=1"
                + "&keyDeserializer=" + StringDeserializer.class.getName()
                + "&valueDeserializer=" + CustomKafkaAvroDeserializer.class.getName()
                + "&allowManualCommit=true"
                + "&autoCommitEnable=false"
                + "&kafkaHeaderDeserializer=#kafkaHeaderDes" //Adicionado o kafkaHeaderDes - bean no MyConfig
                + "&autoOffsetReset=earliest")
                .log("${header.teste}")
                .process(exchange -> {
                    Payment header = exchange.getIn().getHeader("header", Payment.class);
                    System.out.println("HEADER DESERIALIZADO ===> " + header);

                    KafkaManualCommit manual =
                            exchange.getIn().getHeader(KafkaConstants.MANUAL_COMMIT, KafkaManualCommit.class);
                    manual.commitSync();

                })
                .log("${headers} ${body}");
    }
}
