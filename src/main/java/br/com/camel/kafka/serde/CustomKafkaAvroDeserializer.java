package br.com.camel.kafka.serde;

import io.confluent.kafka.serializers.AbstractKafkaAvroDeserializer;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class CustomKafkaAvroDeserializer extends AbstractKafkaAvroDeserializer
        implements Deserializer<Object> {

    private static final String SCHEMA_REGISTRY_URL = "http://localhost:8081";

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        Map<String, Object> configsNew = (Map<String, Object>) configs;
        configsNew.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, SCHEMA_REGISTRY_URL);
        configsNew.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);
        this.configure(new KafkaAvroDeserializerConfig(configsNew));
    }

    @Override
    public Object deserialize(String s, byte[] bytes) {
        return this.deserialize(bytes);
    }

    @Override
    public void close() {
    }
}
