package br.com.camel.kafka.serde;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerializer;
import io.confluent.kafka.serializers.AvroSchemaUtils;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class CustomKafkaAvroSerializer extends AbstractKafkaAvroSerializer
        implements Serializer<Object> {

    private boolean isKey;
    private static final String SCHEMA_REGISTRY_URL = "http://localhost:8081";

    public void configure(Map<String, ?> configs, boolean isKey) {
        this.isKey = isKey;
        Map<String, Object> configsNew = (Map<String, Object>) configs;
        configsNew.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, SCHEMA_REGISTRY_URL);
        this.configure(new KafkaAvroSerializerConfig(configsNew));
    }

    public byte[] serialize(String topic, Object record) {
        return this.serializeImpl(this.getSubjectName(topic, this.isKey, record, AvroSchemaUtils.getSchema(record, this.useSchemaReflection)), record);
    }

    public void close() {
    }
}
