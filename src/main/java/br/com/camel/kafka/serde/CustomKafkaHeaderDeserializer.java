package br.com.camel.kafka.serde;

import br.com.camel.kafka.avro.Payment;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.camel.component.kafka.serde.KafkaHeaderDeserializer;
import org.apache.camel.component.kafka.serde.KafkaHeaderSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class CustomKafkaHeaderDeserializer implements KafkaHeaderDeserializer {

    @Override
    public Object deserialize(String key, byte[] value) {
        if(key.equals("header")) {
            DatumReader<Payment> reader
                    = new SpecificDatumReader<>(Payment.class);
            Decoder decoder;
            try {
                decoder = DecoderFactory.get().jsonDecoder(
                        Payment.getClassSchema(), new String(value));
                return reader.read(null, decoder);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        return value;
    }
}
