package br.com.camel.kafka.serde;

import br.com.camel.kafka.avro.Payment;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.camel.component.kafka.serde.KafkaHeaderSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class CustomKafkaHeaderSerializer implements KafkaHeaderSerializer {

    @Override
    public byte[] serialize(String key, Object value) {
        if(key.equals("header")) {
            DatumWriter<Payment> writer = new SpecificDatumWriter<>(
                    Payment.class);
            byte[] data = new byte[0];
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Encoder jsonEncoder;
            try {
                jsonEncoder = EncoderFactory.get().jsonEncoder(
                        Payment.getClassSchema(), stream);
                writer.write((Payment) value, jsonEncoder);
                jsonEncoder.flush();
                data = stream.toByteArray();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
            return data;
        }
        if (value instanceof String) {
            return ((String) value).getBytes();
        } else if (value instanceof Long) {
            ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
            buffer.putLong((Long) value);
            return buffer.array();
        } else if (value instanceof Integer) {
            ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
            buffer.putInt((Integer) value);
            return buffer.array();
        } else if (value instanceof Double) {
            ByteBuffer buffer = ByteBuffer.allocate(Double.BYTES);
            buffer.putDouble((Double) value);
            return buffer.array();
        } else if (value instanceof Boolean) {
            return value.toString().getBytes();
        } else if (value instanceof byte[]) {
            return (byte[]) value;
        }
        return null;
    }
}
