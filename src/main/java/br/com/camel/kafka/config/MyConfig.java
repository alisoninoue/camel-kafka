package br.com.camel.kafka.config;

import br.com.camel.kafka.serde.CustomKafkaHeaderDeserializer;
import br.com.camel.kafka.serde.CustomKafkaHeaderSerializer;
import org.apache.camel.component.kafka.KafkaComponent;
import org.apache.camel.component.kafka.KafkaConfiguration;
import org.apache.camel.component.kafka.serde.KafkaHeaderDeserializer;
import org.apache.camel.component.kafka.serde.KafkaHeaderSerializer;
import org.apache.camel.spi.ComponentCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class MyConfig {
    @Bean
    ComponentCustomizer<KafkaComponent> myCustomizer(){
        return component -> {
            KafkaConfiguration configuration = component.getConfiguration();
            KafkaConfigurationCustom custom = (KafkaConfigurationCustom) configuration;
            custom.setSpecificAvroReader(true);
            custom.setSchemaRegistryURL("http://localhost:8081");
            Map<String,String> additionalProperties = new HashMap<>();
            additionalProperties.put("schema.registry.ssl", "123");
            additionalProperties.put("schema.registry.keystore", "223");

            component.setConfiguration(custom);
        };
    }

    @Bean(value = "kafkaHeaderSer")
    KafkaHeaderSerializer kafkaHeaderSerializer (){
        return new CustomKafkaHeaderSerializer();
    }
    @Bean(value = "kafkaHeaderDes")
    KafkaHeaderDeserializer kafkaHeaderDeserializer (){
        return new CustomKafkaHeaderDeserializer();
    }
}
